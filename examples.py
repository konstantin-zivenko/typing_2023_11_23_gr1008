import math

pi: float = 3.141592
rr: str

print(__annotations__)


def headline(text: str, align: bool = True) -> str:
    if align:
        return f"{text.title()}\n{"-" * len(text)}"
    else:
        return f"{text.title()}".center(80, "o")


def circumference(radius):
    # type: (float) -> float
    return 2 * math.pi * radius


if __name__ == "__main__":
    print(headline("python type checking", align=True))
